# Chrome Web Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

![](/res/chrome-web-store/search-result.png)

1. [Small promotional tile image](#small-promotional-tile-image)
1. [Title](#title)
1. [Summary](#summary)

### Small promotional tile image

See [en_US](/spec/chrome-web-store/listing-en_US.md#small-promotional-tile-image) instead.

## Listing Overview

![](/res/chrome-web-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

See [en_US](/spec/chrome-web-store/listing-en_US.md#title) instead.

### Summary

See [en_US](/spec/chrome-web-store/listing-en_US.md#summary) instead.

### Detailed Description


```
¿Cansado de los anuncios en Chrome? Adblock Plus es un bloqueador de anuncios fácil de usar que bloquea la publicidad de los vídeos y las redes sociales.

Además de bloquear anuncios, descargar Adblock Plus te permite controlar tu actividad en Internet, bloquear virus y detener el seguimiento.

Adblock Plus cambiará tu forma de ver la web. Para usar Adblock Plus, solo tienes que hacer clic en “Añadir a Chrome”. Haz clic en el icono de ABP para ver cuántos anuncios has bloqueado, cambiar tus ajustes o comunicar un problema.

Adblock Plus apoya la iniciativa Anuncios Aceptables (AA). Son anuncios no intrusivos que no perturban tu experiencia en Internet. Se muestran por defecto para ayudar a mantener páginas web que dependen de ingresos por publicidad a la vez que respetan la experiencia del consumidor. Los usuarios que deseen bloquear todos los anuncios pueden desactivar la función "AA" en cualquier momento. Obtén más información en https://adblockplus.org/es/acceptable-ads

Ten en cuenta que al instalar Adblock Plus para Google Chrome™, recibirás una advertencia indicando que Adblock Plus obtendrá acceso a tu historial de navegación y datos. Se trata de un mensaje estándar; NUNCA recopilamos información del usuario.

Al descargar e instalar esta extensión, aceptas nuestros Términos de uso https://adblockplus.org/terms y nuestra Política de privacidad https://adblockplus.org/privacy.

Adblock Plus también está disponible para Firefox y Edge.

************

Los anuncios de nuevas versiones e históricos de cambios se pueden encontrar en: http://adblockplus.org/releases/

¿Tienes problemas con la extensión? Prueba a volver a cargar Google Chrome™, o bien abre los ajustes de Adblock Plus, haz clic en la pestaña Avanzado y, a continuación, haz clic en Actualizar todas las listas de filtros.

¿Has encontrado un problema? Echa un vistazo a los problemas comunes en https://adblockplus.org/es/bugs o visita nuestro foro en https://adblockplus.org/forum/viewforum.php?f=10

Si te sientes aventurero, también puedes probar las versiones de desarrollo para obtener las últimas funciones de Adblock Plus para Google Chrome™. Estas se actualizan individualmente con ajustes independientes. https://adblockplus.org/development-builds
```

## Screenshots

![](/res/chrome-web-store/listing-screenshots.png)

1. Cycles throught all uploaded screenshots (max 5) below.
    - [Screeenshot-1](#screeenshot-1)
    - [Screeenshot-2](#screeenshot-2)
    - [Screeenshot-3](#screeenshot-3)
    - [Screeenshot-4](#screeenshot-4)
    - [Screeenshot-5](#screeenshot-5)

### Screeenshot-1

![](/res/chrome-web-store/screenshot-1_ES.png)

1. `Bloquea anuncios molestos en Internet, incluidos los anuncios de Facebook y los de vídeos de YouTube`
1. `Adblock Plus`
1. `Bloquear anuncios en:`
1. `9 en esta página`
1. `24 en total`
1. `Informar de un problema`
1. `Bloquear elemento`
1. `¿Quieres tener Adblock Plus en el móvil?`

### Screeenshot-2

![](/res/chrome-web-store/screenshot-2_ES.png)

1. `Bloquea los anuncios que siguen tu actividad en línea y mantén tus datos seguros`
1. `Noticias`

### Screeenshot-3

![](/res/chrome-web-store/screenshot-3_ES.png)

1. `Bloquea virus y malware ocultos en anuncios`
1. `Noticias`

### Screeenshot-4

![](/res/chrome-web-store/screenshot-4_ES.png)

1. `Apoya a las páginas web en las que confías incluyéndolas en una lista blanca`
1. `Configuración`
1. `Adblock Plus Ajustes`
1. `General`
1. `Páginas web de la lista blanca`
1. `Avanzado`
1. `Ayuda`
1. `Páginas web de la lista blanca`
1. `Has desactivado el bloqueo de anuncios en estas páginas web y, por tanto, verás anuncios. Más información`
1. `AÑADIR PÁGINA WEB`

### Screeenshot-5

![](/res/chrome-web-store/screenshot-5_ES.png)

1. `Los Anuncios Aceptables apoyan automáticamente a las páginas web que te gustan`
1. `Página web`
