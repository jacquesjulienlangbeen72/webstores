# Chrome Web Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

![](/res/chrome-web-store/search-result.png)

1. [Small promotional tile image](#small-promotional-tile-image)
1. [Title](#title)
1. [Summary](#summary)

### Small promotional tile image

See [en_US](/spec/chrome-web-store/listing-en_US.md#small-promotional-tile-image) instead.

## Listing Overview

![](/res/chrome-web-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

See [en_US](/spec/chrome-web-store/listing-en_US.md#title) instead.

### Summary

See [en_US](/spec/chrome-web-store/listing-en_US.md#summary) instead.

### Detailed Description


```
Du hast keine Lust mehr auf Werbung bei Chrome? Adblock Plus ist ein leicht zu verwendender Adblocker, der Werbung in Form von Videos und sozialen Medien blockt.

Aber Adblock Plus blockt nicht nur unerwünschte Werbung, sondern gibt dir auch die Kontrolle über dein Internet, stoppt Computerviren und verhindert Tracking.

Adblock Plus wird verändern, wie du das Web siehst. Um Adblock Plus zu nutzen, klicke einfach auf „Zu Chrome hinzufügen“. Klicke auf das ABP-Symbol, um zu sehen, wie viele Werbungen du geblockt hast, um deine Einstellungen zu ändern oder um einen Fehler zu melden.

Adblock Plus unterstützt die Acceptable Ads (AA) Initiative. Dabei handelt es sich um nicht störende Werbung, die dein Interneterlebnis nicht unterbrechen. Diese werden standardmäßig angezeigt, um Websites zu unterstützen, die auf Werbeeinnahmen angewiesen sind, und berücksichtigen die Kundenerfahrung. „AA“ kann von Benutzern, die alle Arten von Werbung blockieren möchten, zu jeder Zeit ausgeschaltet werden. Unter https://adblockplus.org/de/acceptable-ads erfährst du mehr

Bitte beachte: Bei der Installation von Adblock Plus für Google Chrome™ erhältst du einen Warnhinweis, dass Adblock Plus Zugriff auf deinen Browserverlauf und deine Daten erhält. Es handelt sich dabei um eine Standardnachricht – wir sammeln NIE jegliche Informationen unserer Benutzer.

Durch den Download und die Installation dieser Erweiterung stimmst du den Nutzungsbedingungen unter https://adblockplus.org/de/terms und unseren Datenschutzerklärung unter https://adblockplus.org/de/privacy zu.
Adblock Plus gibt es auch für Firefox und Edge.
************

Release-Ankündigungen und die Änderungshistorie findest du unter: http://adblockplus.org/releases/
Du hast Probleme mit der Erweiterung? Versuche, Google Chrome™ neu zu laden und/oder die Einstellungen von Adblock Plus zu öffnen, klicke dann auf den Reiter „Erweitert“ und anschließend auf „Alle Filterlisten aktualisieren“.

Du hast einen Fehler gefunden? Sieh dir unter https://adblockplus.org/de/bugs bereits bekannte Probleme an oder besuche unser Forum auf https://adblockplus.org/forum/viewforum.php?f=10

Wenn du Lust auf ein kleines Abenteuer hast, kannst du auch die Entwicklungsversionen ausprobieren, um die neuesten Funktionen von Adblock Plus für Google Chrome™ zu erhalten. Diese Versionen werden mit unabhängigen Einstellungen separat aktualisiert. https://adblockplus.org/development-builds
```

## Screenshots

![](/res/chrome-web-store/listing-screenshots.png)

1. Cycles throught all uploaded screenshots (max 5) below.
    - [Screeenshot-1](#screeenshot-1)
    - [Screeenshot-2](#screeenshot-2)
    - [Screeenshot-3](#screeenshot-3)
    - [Screeenshot-4](#screeenshot-4)
    - [Screeenshot-5](#screeenshot-5)

### Screeenshot-1

![](/res/chrome-web-store/screenshot-1_DE.png)

1. `Blocke lästige Werbung im Internet einschließlich Facebook- und YouTube-Videowerbung`
1. `Adblock Plus`
1. `Werbung blocken für:`
1. `9 auf dieser Seite geblockt`
1. `24 insgesamt geblockt`
1. `Problem melden`
1. `Element blockieren`
1. `Hast du Interesse an Adblock Plus fürs Handy?`

### Screeenshot-2

![](/res/chrome-web-store/screenshot-2_DE.png)

1. `Blockiere Werbung, die dich verfolgt und sichere deine Daten`
1. `Nachrichten`

### Screeenshot-3

![](/res/chrome-web-store/screenshot-3_DE.png)

1. `Blocke in Werbung versteckte Viren und Malware`
1. `Nachrichten`

### Screeenshot-4

![](/res/chrome-web-store/screenshot-4_DE.png)

1. `Unterstütze Websites, denen du vertraust, indem du sie auf die Whitelist setzt`
1. `Einstellungen`
1. `Adblock Plus Einstellungen`
1. `Allgemein`
1. `Websites auf der Whitelist`
1. `Erweitert`
1. `Hilfe`
1. `Websites auf der Whitelist`
1. `Du hast das Blocken von Werbung auf diesen Websites deaktiviert und wirst auf ihnen Anzeigen sehen. Erfahre mehr`
1. `WEBSITE HINZUFÜGEN`

### Screeenshot-5

![](/res/chrome-web-store/screenshot-5_DE.png)

1. `Acceptable Ads unterstützt automatisch die Websites, die du magst`
1. `Website`
