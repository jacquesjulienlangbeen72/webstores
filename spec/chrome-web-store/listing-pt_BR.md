# Chrome Web Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

![](/res/chrome-web-store/search-result.png)

1. [Small promotional tile image](#small-promotional-tile-image)
1. [Title](#title)
1. [Summary](#summary)

### Small promotional tile image

See [en_US](/spec/chrome-web-store/listing-en_US.md#small-promotional-tile-image) instead.

## Listing Overview

![](/res/chrome-web-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

See [en_US](/spec/chrome-web-store/listing-en_US.md#title) instead.

### Summary

See [en_US](/spec/chrome-web-store/listing-en_US.md#summary) instead.

### Detailed Description


```
Cansado de anúncios no Chrome? O Adblock Plus é um bloqueador de anúncios fácil de usar que bloqueia anúncios em vídeo e nas mídias sociais.

Ao fazer download do Adblock Plus, além de obter um bloqueador de anúncios, você pode assumir o controle da sua internet, bloquear vírus e rastreamentos.

O Adblock Plus mudará o seu modo de ver a internet. Para usar o Adblock Plus, basta clicar em "Usar no Chrome". Clique no ícone do ABP para ver quantos anúncios você bloqueou, alterar suas configurações ou reportar um bug.

O Adblock Plus apoia a iniciativa de Anúncios Aceitáveis (AA). Esses são anúncios não intrusivos que não atrapalham a sua experiência na internet. São exibidos por padrão para oferecer suporte a sites que dependem da receita de publicidade, ao mesmo tempo respeitando a experiência do usuário. A função "AA" pode ser desativada a qualquer momento para os usuários que desejam bloquear todos os anúncios. Saiba mais em https://adblockplus.org/pt_BR/acceptable-ads

Observação: quando instalar o Adblock Plus para o Google Chrome™, você receberá um aviso de que o Adblock Plus terá acesso ao seu histórico de navegação e dados. Esta é uma mensagem padrão - NUNCA coletamos qualquer informação do usuário.

Ao baixar e instalar esta extensão, você concorda com nossos Termos de Uso https://adblockplus.org/terms e nossa Política de Privacidade https://adblockplus.org/privacy.

O Adblock Plus também está disponível para Firefox e Edge.

************

Os anúncios de lançamentos e registros de todas as alterações no sistema podem ser encontrados em: http://adblockplus.org/releases/

Algum problema com a extensão? Tente recarregar o Google Chrome™ e/ou abrir as configurações do Adblock Plus, clicando na guia Avançado e em Atualizar todas as listas de filtros.

Encontrou um bug? Veja os problemas conhecidos em https://adblockplus.org/bugs ou visite o nosso fórum em https://adblockplus.org/forum/viewforum.php?f=10

Se gostar de novas experiências, você poderá sempre testar os novos desenvolvimentos e obter os recursos mais atualizados do Adblock Plus para o Google Chrome™. Esses desenvolvimentos são atualizados separadamente com configurações independentes. https://adblockplus.org/development-builds
```

## Screenshots

![](/res/chrome-web-store/listing-screenshots.png)

1. Cycles throught all uploaded screenshots (max 5) below.
    - [Screeenshot-1](#screeenshot-1)
    - [Screeenshot-2](#screeenshot-2)
    - [Screeenshot-3](#screeenshot-3)
    - [Screeenshot-4](#screeenshot-4)
    - [Screeenshot-5](#screeenshot-5)

### Screeenshot-1

![](/res/chrome-web-store/screenshot-1_PT.png)

1. `Bloqueia os anúncios irritantes na web, incluindo anúncios no Facebook e anúncios de vídeo no YouTube`
1. `Adblock Plus`
1. `Bloquear anúncios em:`
1. `9 bloqueados neste site`
1. `24 bloqueados no total`
1. `Reportar problema`
1. `Bloquear elemento`
1. `Você tem interesse em adicionar o Adblock Plus no seu smartphone?`

### Screeenshot-2

![](/res/chrome-web-store/screenshot-2_PT.png)

1. `Bloqueia os anúncios que te rastreiam e matém seus dados seguros`
1. `Notícias`

### Screeenshot-3

![](/res/chrome-web-store/screenshot-3_PT.png)

1. `Bloqueia vírus e malware ocultos nos anúncios`
1. `Notícias`

### Screeenshot-4

![](/res/chrome-web-store/screenshot-4_PT.png)

1. `Ajude os sites que você confia e os adicione na lista branca`
1. `Definições`
1. `Adblock Plus Definições`
1. `Geral`
1. `Sites na lista branca`
1. `Avançado`
1. `Ajuda`
1. `Sites na lista branca`
1. `Você desativou o bloqueio de anúncios nestes sites e, como tal, verá anúncios nestes. Saiba mais.`
1. `Adicionar site`

### Screeenshot-5

![](/res/chrome-web-store/screenshot-5_PT.png)

1. `Os Anúncios Aceitáveis ajudam automaticamente os sites que você gosta`
1. `Site`
