# Chrome Web Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

![](/res/chrome-web-store/search-result.png)

1. [Small promotional tile image](#small-promotional-tile-image)
1. [Title](#title)
1. [Summary](#summary)

### Small promotional tile image

See [en_US](/spec/chrome-web-store/listing-en_US.md#small-promotional-tile-image) instead.

## Listing Overview

![](/res/chrome-web-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

See [en_US](/spec/chrome-web-store/listing-en_US.md#title) instead.

### Summary

See [en_US](/spec/chrome-web-store/listing-en_US.md#summary) instead.

### Detailed Description


```
Устали от рекламы в Chrome? Adblock Plus — удобный блокировщик рекламы, который блокирует видеорекламу и рекламу в социальных сетях.

Помимо блокировщика рекламы, загрузив Adblock Plus, вы получаете возможность контролировать свою работу в интернете, блокировать вирусы и прекратить отслеживание.

Adblock Plus изменит ваше восприятие всемирной сети. Чтобы начать пользоваться Adblock Plus, просто щелкните “Установить”. Щелкните по значку ABP, чтобы увидеть количество заблокированных рекламных объявлений, изменить настройки или отправить отчет об ошибке.

Adblock Plus поддерживает инициативу "Допустимая реклама" (Acceptable Ads - AA). Речь идет о рекламе, не мешающей вашей работе в интернете. Она демонстрируется по умолчанию для поддержки сайтов, зависящих от рекламной прибыли, не создавая при этом неудобств пользователям. "AA" можно отключить в любое время, если пользователь желает заблокировать всю рекламу. Подробности по ссылке: https://adblockplus.org/acceptable-ads

Обратите внимание: при установке Adblock Plus для Google Chrome™ вы увидите предупреждение о том, что Adblock Plus получит доступ к вашим данным и истории поисковых запросов. Это стандартное сообщение — мы НИКОГДА не собираем информацию о пользователях.

Загружая и устанавливая данное расширение, вы принимаете наши Условия использования https://adblockplus.org/terms и Политику конфиденциальности https://adblockplus.org/privacy.

Adblock Plus также доступен для Firefox и Edge.

************

Сообщения о выпуске и перечень изменений можно найти по ссылке: http://adblockplus.org/releases/

Проблемы с расширением? Попробуйте перезагрузить Google Chrome™ и/или открыть настройки Adblock Plus, щелкнуть по вкладке "Расширенные" и нажать "Обновить все списки фильтров".

Нашли ошибку? Ознакомьтесь с известными проблемами по ссылке: https://adblockplus.org/ru/bugs или зайдите на наш форум: https://adblockplus.org/forum/viewforum.php?f=10

Если вам хочется приключений, то вы всегда можете попробовать тестовые сборки, чтобы получить доступ к самым последним функциям Adblock Plus для Google Chrome™. Эти сборки обновляются отдельно и имеют автономные настройки. https://adblockplus.org/development-builds
```

## Screenshots

![](/res/chrome-web-store/listing-screenshots.png)

1. Cycles throught all uploaded screenshots (max 5) below.
    - [Screeenshot-1](#screeenshot-1)
    - [Screeenshot-2](#screeenshot-2)
    - [Screeenshot-3](#screeenshot-3)
    - [Screeenshot-4](#screeenshot-4)
    - [Screeenshot-5](#screeenshot-5)

### Screeenshot-1

![](/res/chrome-web-store/screenshot-1_RU.png)

1. `Блокируйте назойливую рекламу в сети, включая объявления на Facebook и рекламные ролики на YouTube`
1. `Adblock Plus`
1. `Блокируйте рекламу на:`
1. `9 на данной странице`
1. `24 всего`
1. `Сообщить о проблеме`
1. `Заблокировать элемент`
1. `Интересен ли вам Adblock Plus для мобильных платформ?`

### Screeenshot-2

![](/res/chrome-web-store/screenshot-2_RU.png)

1. `Блокируйте рекламу, которая отслеживает вашу активность, и защитите ваши данные`
1. `Новости`

### Screeenshot-3

![](/res/chrome-web-store/screenshot-3_RU.png)

1. `Блокируйте вирусы и вредоносное ПО, скрытые в рекламе`
1. `Новости`

### Screeenshot-4

![](/res/chrome-web-store/screenshot-4_RU.png)

1. `Поддерживайте сайты, которым вы доверяете, добавляя их в "белый список"`
1. `Настройки`
1. `Adblock Plus Настройки`
1. `Общие`
1. `Белый список сайтов`
1. `Расширенные`
1. `Справка`
1. `Белый список сайтов`
1. `Вы выключили блокировку рекламы на этих сайтах и, следовательно, будете видеть на них рекламу. Узнать больше`
1. `ДОБАВИТЬ САЙТ`

### Screeenshot-5

![](/res/chrome-web-store/screenshot-5_RU.png)

1. `Допустимая реклама автоматически размещается на ваших любимых сайтах`
1. `Сайт`
