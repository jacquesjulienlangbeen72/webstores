# Chrome Web Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

![](/res/chrome-web-store/search-result.png)

1. [Small promotional tile image](#small-promotional-tile-image)
1. [Title](#title)
1. [Summary](#summary)

### Small promotional tile image

![](/res/chrome-web-store/small-promotional-tile-image.png)

## Listing Overview

![](/res/chrome-web-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

`Adblock Plus - free ad blocker`

### Summary

`Download Adblock Plus, one of the world's most popular ad blockers.`

### Detailed Description


```
The Adblock Plus for Chrome™ ad blocker has been downloaded over 500 million times and is one of the most popular and trusted on the market. Users get fast, sleek ad-blocking capabilities to enjoy sites like YouTube™ interruption free. 

✓ Block annoying video ads and banners
✓ Block pop ups
✓ Stop tracking and give yourself more privacy
✓ Fight off destructive malvertising that can hide in ads
✓ Give yourself faster browsing (as resources are blocked from loading)
✓ Customize features, like whitelisting for favorite sites
✓ Get free and constant support
✓ Give yourself simply more control of the experience you want

Adblock Plus for Chrome™ is the adblocker to help you fall in love with the internet again, and gives you the chance to customize your experience.

ⓘ To use Adblock Plus, just click on "Add to Chrome". Click on the ABP icon to see how many ads you've blocked, to change your settings, or report an error. It’s that easy!

Adblock Plus supports the Acceptable Ads (AA) (www.acceptableads.com) initiative by default, a project that looks for a middle way, to support websites that rely on advertising revenue yet take into account the customer experience. If you do not wish to see Acceptable Ads, this can be turned off at any time: https://adblockplus.org/de/acceptable-ads#optout.

What some of our users say:

Joshua Blackerby - ★★★★★
“Awesome, It blocks every ad on every site, also works on social media.”

khorne, The blood god - ★★★★★
“So far, adblock plus has worked perfectly for YouTube watching and other sites.”

Matej K - ★★★★★
“Does exactly what it says. No more annoying pop-ups, accidental clicks on malware ads and nothing getting in your way of enjoying the internet. It's easy to turn off if a site blocks it or if you want to support someone through ad revenue.”

Peter Haralanov - ★★★★★
“I am using it for years now. It does what it's supposed to.
One of the most useful extensions ever!”

************

Please Note: When installing Adblock Plus for Google Chrome™, you will receive a warning that Adblock Plus will receive access to your browsing history and data. This is a standard message - we NEVER collect any user information.

By downloading and installing this extension, you agree to our Terms of Use https://adblockplus.org/terms and our Privacy Policy https://adblockplus.org/privacy.

Adblock Plus is also available for Firefox and Edge.

************

Release announcements and changelogs can be found at: http://adblockplus.org/releases/

Problem with the extension? Try reloading Google Chrome™ and/or opening the Adblock Plus settings, clicking the Advanced tab, and clicking Update all filter lists.

Found a bug? Check known issues at https://adblockplus.org/bugs or visit our forum at https://adblockplus.org/forum/viewforum.php?f=10

If you feel adventurous you can always try out the development builds to get the latest features of Adblock Plus for Google Chrome™. These builds are updated separately with independent settings. https://adblockplus.org/development-builds
```

## Screenshots

![](/res/chrome-web-store/listing-screenshots.png)

1. Cycles throught all uploaded screenshots (max 5) below.
    - [Screeenshot-1](#screeenshot-1)
    - [Screeenshot-2](#screeenshot-2)
    - [Screeenshot-3](#screeenshot-3)
    - [Screeenshot-4](#screeenshot-4)
    - [Screeenshot-5](#screeenshot-5)

### Screeenshot-1

![](/res/chrome-web-store/screenshot-1_EN.png)

1. `Block annoying ads on the web including Facebook ads and YouTube video ads`
1. `Adblock Plus`
1. `Block ads on:`
1. `9 blocked on this page`
1. `24 blocked in total`
1. `Report issue`
1. `Block element`
1. `Interested in Adblock Plus on mobile?`

### Screeenshot-2

![](/res/chrome-web-store/screenshot-2_EN.png)

1. `Block ads that track you and keep your data safe`
1. `News`

### Screeenshot-3

![](/res/chrome-web-store/screenshot-3_EN.png)

1. `Block viruses and malware hidden in advertisements`
1. `News`

### Screeenshot-4

![](/res/chrome-web-store/screenshot-4_EN.png)

1. `Support websites you trust by whitelisting them`
1. `Settings`
1. `Adblock Plus Settings`
1. `General`
1. `Whitelisted websites`
1. `Advanced`
1. `Help`
1. `Whitelisted websites`
1. `You’ve turned off ad blocking on these websites and, therefore, will see ads on them. Learn more`
1. `ADD WEBSITE`

### Screeenshot-5

![](/res/chrome-web-store/screenshot-5_EN.png)

1. `Acceptable Ads automatically support websites you love`
1. `Website`
