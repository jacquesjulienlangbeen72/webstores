# Chrome Web Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

![](/res/chrome-web-store/search-result.png)

1. [Small promotional tile image](#small-promotional-tile-image)
1. [Title](#title)
1. [Summary](#summary)

### Small promotional tile image

See [en_US](/spec/chrome-web-store/listing-en_US.md#small-promotional-tile-image) instead.

## Listing Overview

![](/res/chrome-web-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

See [en_US](/spec/chrome-web-store/listing-en_US.md#title) instead.

### Summary

See [en_US](/spec/chrome-web-store/listing-en_US.md#summary) instead.

### Detailed Description


```
Vous en avez assez de voir des publicités dans Chrome ? Adblock Plus est un bloqueur de publicités facile à utiliser qui désactive la publicité vidéos et des réseaux sociaux.

Plus qu'un simple bloqueur de publicité, Adblock Plus vous permet de prendre totalement le contrôle de votre connexion Internet, de bloquer les virus et de mettre fin au pistage.

Avec Adblock Plus, votre vision du Web va changer. Pour utiliser Adblock Plus, cliquez simplement sur « Ajouter à Chrome ». Cliquez sur l'icône ABP pour voir combien d'annonces vous avez bloqué, modifier vos paramètres ou signaler un problème.

Adblock Plus soutient l'initiative Publicité Acceptable (Acceptable Ads - AA) qui favorise les publicités non intrusives ne perturbant pas votre expérience du Web. Les annonces s'affichent par défaut pour soutenir les sites Web qui ont besoin de recettes publicitaires pour vivre, tout en respectant l'expérience des consommateurs. « AA » peut être désactivé à tout moment pour les utilisateurs qui souhaitent bloquer toutes les publicités. Pour plus d'informations, consultez le site https://adblockplus.org/fr/acceptable-ads

Remarque : lors de l'installation d'Adblock Plus pour Google Chrome™, un avertissement s'affiche pour indiquer qu'Adblock Plus aura accès à votre historique et à vos données de navigation. Il s'agit d'un message standard. En effet, nous ne collectons JAMAIS aucune information sur les utilisateurs.

En téléchargeant et en installant cette extension, vous acceptez nos Conditions d'utilisation https://adblockplus.org/fr/terms et notre Politique de protection de la vie privée https://adblockplus.org/fr/privacy.

Adblock Plus est également disponible pour les navigateurs Firefox et Edge.

************

Les annonces de diffusion et les journaux des modifications sont consultables sur le site http://adblockplus.org/releases/

Vous rencontrez des problèmes avec l'extension ? Rechargez Google Chrome™ et/ou ouvrez les paramètres d'Adblock Plus, cliquez sur l'onglet Paramètres avancés et cliquez sur Mettre à jour toutes les listes de filtres.

Vous avez trouvé un bug ? Consultez les problèmes connus sur https://adblockplus.org/fr/bugs ou visitez notre forum sur le site https://adblockplus.org/forum/viewforum.php?f=10

Si l'expérience vous tente, vous pouvez tester en permanence les versions en cours de développement afin d'obtenir les dernières fonctionnalités d'Adblock Plus pour Google Chrome™. Ces versions sont mises à jour séparément et avec des paramètres indépendants. Consultez le site https://adblockplus.org/development-builds
```

## Screenshots

![](/res/chrome-web-store/listing-screenshots.png)

1. Cycles throught all uploaded screenshots (max 5) below.
    - [Screeenshot-1](#screeenshot-1)
    - [Screeenshot-2](#screeenshot-2)
    - [Screeenshot-3](#screeenshot-3)
    - [Screeenshot-4](#screeenshot-4)
    - [Screeenshot-5](#screeenshot-5)

### Screeenshot-1

![](/res/chrome-web-store/screenshot-1_FR.png)

1. `Bloquez les publicités agaçantes sur le Web, y compris les vidéos publicitaires de Facebook et YouTube`
1. `Adblock Plus`
1. `Blocage des publicités activé :`
1. `9 publicités bloquées sur cette page`
1. `24 publicités bloquées au total`
1. `Signaler un problème`
1. `Bloquer l'élément`
1. `Vous êtes intéressé(e) par Adblock Plus pour le mobile ?`

### Screeenshot-2

![](/res/chrome-web-store/screenshot-2_FR.png)

1. `Bloquez les publicités qui vous suivent et sécurisez vos données`
1. `Actualités`

### Screeenshot-3

![](/res/chrome-web-store/screenshot-3_FR.png)

1. `Bloquez les virus et logiciels malveillants dissimulés dans les publicités`
1. `Actualités`

### Screeenshot-4

![](/res/chrome-web-store/screenshot-4_FR.png)

1. `Soutenez les sites Web auxquels vous faites confiance en les mettant sur liste blanche`
1. `Paramètres`
1. `Adblock Plus Paramètres`
1. `Paramètres généraux`
1. `Sites Web sur liste blanche`
1. `Paramètres avancés`
1. `Aide`
1. `Sites Web sur liste blanche`
1. `Comme vous avez désactivé le blocage des publicités sur ces sites web, vous verrez des publicités lorsque vous les consultez. Plus d'infos`
1. `AJOUTER UN SITE WEB`

### Screeenshot-5

![](/res/chrome-web-store/screenshot-5_FR.png)

1. `Les Publicités Acceptables soutiennent les sites Web que vous aimez`
1. `Site Web`
