# Chrome Web Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

![](/res/chrome-web-store/search-result.png)

1. [Small promotional tile image](#small-promotional-tile-image)
1. [Title](#title)
1. [Summary](#summary)

### Small promotional tile image

See [en_US](/spec/chrome-web-store-listing-en_US.md#small-promotional-tile-image) instead.

## Listing Overview

![](/res/chrome-web-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

`Adblock Plus - 的免费广告拦截程序`

### Summary

See [en_US](/spec/chrome-web-store-listing-en_US.md#summary) instead.

### Detailed Description


```
享受没有恼人广告的网络世界。

Adblock Plus for Google Chrome 会阻挡：

· 横幅
· YouTube 视频广告
· Facebook 广告
· 弹出窗口
· 所有其他显眼的广告

Adblock Plus 是世界上最流行的浏览器扩展，世界各地有数百万用户在使用它。这是一个社区驱动的开源项目，有数百名志愿者为 Adblock Plus 的成功作出了贡献，以实现所有烦人的广告被自动阻挡。

请注意：当安装 Adblock Plus for Chrome 时，您的浏览器会显示一个警告，它说 Adblock Plus for Chrome 会访问您的浏览历史和数据。这是一个标准的消息，我们从不、绝不会收集任何信息！

最近，Adblock Plus 社区提出了可接受广告概念。通过允许一些小的和静态的广告，您可以支持那些依赖于广告，但选择一些非侵入式广告的网站。此功能可以在任何时候禁用，到 http://adblockplus.org/zh_CN/acceptable-ads 可以了解更多细节。

***********
发行公告和变更日志可以在这里找到: http://adblockplus.org/releases/

遇到问题？试试重新启动 Chrome 和/或在 Adblock Plus 选项中点击“立即更新”。

发现了bug？我们已知的一些问题: http://adblockplus.org/zh_CN/known-issues-chrome 。如果您发现了更多或需要帮助，访问我们的论坛: https://adblockplus.org/forum/viewforum.php?f=10

*************
喜欢冒险的感觉吗？试试开发版本来始终得到 Chrome Adblock Plus 的最新特性: http://adblockplus.org/en/development-builds （分别更新，独立的设置）。
```

## Screenshots

See [en_US](/spec/chrome-web-store-listing-en_US.md#screenshots) instead.