# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)

### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`使用这个免费的 Firefox 版本广告拦截程序来拦截 Facebook、YouTube 和所有其他网站上的烦人的广告。`

`Adblock Plus 拦截所有烦人的广告,并通过默认（可配置）不拦截不显眼的广告来支持网站。`

### Detailed Description

```
<strong>获取用于火狐浏览器的免费Adblocker。至今已有近5亿次下载！</strong>

✓ 拦截恼人的广告及弹出窗口
✓ 拦截YouTube等网站的视频广告
✓ 提高网页加载速度
✓ 降低恶意广告感染风险
✓ 阻止跟踪器跟随您的在线活动以保护您的隐私
✓ 拦截社交媒体图标跟踪

广告拦截器的附加功能可以让您将您喜爱的网站加入白名单来支持这些网站、以及添加或创建您自己的过滤器，并拦截社交媒体图标跟踪。

Adblock Plus支持可接受广告倡议。可接受广告将默认显示，这有助于支持那些依赖于广告收入但选择仅显示非干扰性广告的网站。如果用户想要拦截所有广告，可随时禁用这一功能。这一倡议让内容创建方可以通过作品获取收入，并有助于为用户、广告商和内容创建方共同营造公平且可持续的环境。欲了解更多信息，请访问：https://adblockplus.org/acceptable-ads

下载并安装本扩展即表示您同意我们的<a href="https://adblockplus.org/terms">使用条款</a>和我们的<a href="https://adblockplus.org/privacy">隐私政策</a>。
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.    

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`轻松移除网络上（包括 Facebook 和 YouTube）烦人的广告`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`阻止网站追踪您并跟踪您的在线活动`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`通过拦截广告中隐藏的病毒和恶意软件来保护您`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`把您要支持的网站列入白名单`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`允许不烦人或不具破坏性的可接受广告来支持网站`