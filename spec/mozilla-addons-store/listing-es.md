# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Bloquea anuncios molestos en páginas como Facebook o YouTube con este bloqueador de anuncios gratuito para Firefox.`

`Adblock Plus bloquea todos los anuncios molestos y ayuda a páginas web al permitir por defecto (configurable) anuncios no intrusivos.`

### Detailed Description

```
<strong>Obtén el bloqueador de anuncios gratuito para Firefox. ¡Con casi 500 millones de descargas hasta la fecha!</strong>

✓ Bloquea ventanas emergentes y anuncios molestos
✓ Bloquea los anuncios en vídeos en sitios como YouTube
✓ Acelera los tiempos de carga de las páginas
✓ Reduce el riesgo de infección por "malvertising"
✓ Protege tu privacidad evitando que los rastreadores sigan tu actividad en línea
✓ Bloquea el seguimiento de los iconos de redes sociales

Las funciones adicionales del bloqueador de anuncios te permiten apoyar a tus sitios web favoritos incluyéndolos en una lista blanca, añadir o crear tus propios filtros, y bloquear el seguimiento de los iconos de redes sociales.

Adblock Plus apoya la iniciativa Anuncios Aceptables. Los anuncios aceptables se muestran por defecto, lo que ayuda a apoyar a sitios web que dependen de los ingresos procedentes de la publicidad, pero optan por mostrar únicamente anuncios no intrusivos. Los usuarios que deseen bloquear todos los anuncios pueden desactivar esta funcionalidad en cualquier momento. La iniciativa permite a los productores de contenido recibir monetización por su trabajo y ayuda a crear un entorno justo y sostenible para usuarios, anunciantes y creadores por igual. Obtén más información en https://adblockplus.org/acceptable-ads

Al descargar e instalar esta extensión, aceptas nuestros <a href="https://adblockplus.org/terms">Términos de uso</a> y nuestra <a href="https://adblockplus.org/privacy">Política de privacidad</a>.
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.    

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`Deshazte con facilidad de anuncios molestos en Internet, ya sea en Facebook o YouTube`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Detén el seguimiento de tu actividad en línea por parte de los sitios web`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`Te protege bloqueando los virus y el malware que se esconden en los anuncios`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Añade a la lista blanca las páginas web a las que quieras ayudar`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Ayuda a los sitios web permitiendo los anuncios aceptables que no sean molestos ni interrumpan la experiencia del usuario`


