# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)

### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Blokkeer irritante advertenties op Facebook, YouTube en alle andere websites met deze gratis adblocker voor Firefox.`

`Adblock Plus blokkeert irritante advertenties en steunt websites door niet-opdringerige advertenties niet te blokkeren (instelbaar).`

### Detailed Description

```
<strong>Download de gratis Adblocker voor Firefox. Tot nu toe bijna 500 miljoen downloads!</strong>

✓ Blokkeer vervelende advertenties en pop-ups
✓ Blokkeer video-advertenties op sites als YouTube
✓ Verbeter de laadsnelheid van pagina's
✓ Verminder het risico op "malvertising"-infecties
✓ Bescherm je privacy door trackers die je activiteiten online volgen te stoppen
✓ Blokkeer tracking van social media-iconen

Met de extra functies van de adblocker kun je heel eenvoudig je favoriete websites steunen door ze te whitelisten, je eigen filters te creëren en tracking van social media-iconen blokkeren.

Adblock Plus steunt het Acceptabele Advertenties-initiatief. Standaard worden acceptabele advertenties getoond. Dit steunt websites die op advertentieinkomsten vertrouwen maar ervoor kiezen om niet-opdringerige advertenties te tonen. Dit kan op elk moment worden uitgeschakeld door gebruikers die alle advertenties willen blokkeren. Dankzij dit initiatief kunnen producenten van inhoud inkomsten genereren voor hun werk en wordt er een omgeving van rechtvaardigheid en duurzaamheid gecreëerd voor gebruikers, adverteerders en makers. Meer informatie op https://adblockplus.org/acceptable-ads

Door deze extensie te downloaden en installeren, ga je akkoord met onze <a href="https://adblockplus.org/terms">Gebruiksvoorwaarden</a> en ons <a href="https://adblockplus.org/privacy">Privacybeleid</a>.
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.    

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`Verwijder eenvoudig hinderlijke advertenties op het web, waaronder op Facebook en YouTube`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Zorg dat websites u en uw online activiteiten niet meer kunnen volgen`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`Beschermt u door in advertenties verborgen virussen en malware te blokkeren`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Plaats websites die u wilt steunen op de uitzonderingslijst`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Ondersteun websites door Acceptabele Advertenties toe te staan die niet hinderlijk of storend zijn`