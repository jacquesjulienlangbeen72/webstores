# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

`Adblock Plus`

### Summary

`One of the most popular free ad blockers for Firefox. Block annoying ads on sites like Facebook, YouTube and all other websites.`

`Adblock Plus blocks all annoying ads, and supports websites by not blocking unobtrusive ads by default (configurable).`

**Important detail:**

For English, the summary above applies. For all other languages, due to the character limit that we have, we've translated the text below:

`Block annoying ads on sites like Facebook, YouTube and all other websites with this free ad blocker for Firefox.`

`Adblock Plus blocks all annoying ads, and supports websites by not blocking unobtrusive ads by default (configurable).`

### Detailed Description


```
Get the <strong>free ad blocker for Firefox</strong>. With almost 500 million downloads to date!

✓ Block annoying ads and popups
✓ Block video ads on sites like YouTube 
✓ Speed-up loading time on pages
✓ Reduce risk of "malvertising" infections
✓ Protect your privacy by stopping trackers from following your online activity
✓ Block social media icons tracking

The ad blocker's additional features enable you to easily support your favorite websites by whitelisting them, to add or create your own filters, and to block social media icons tracking.

Adblock Plus supports the Acceptable Ads initiative. Acceptable Ads are shown by default, which helps support websites that rely on advertising revenue but choose to only display nonintrusive ads. This can be disabled at any time for users who wish to block all ads. The initiative allows content producers to receive monetization for their work and helps create an environment of fairness and sustainability for user, advertiser, and creator alike. <a href="https://adblockplus.org/acceptable-ads">Learn more</a>

By downloading and installing this extension, you agree to our <a href="https://adblockplus.org/terms">Terms of Use</a> and our <a href="https://adblockplus.org/privacy">Privacy Policy</a>.

```

## Screenshots

![](/res/mozilla-addons-store/listing-screenshots.png)

Clicking on one of the screenshots opens up an interstitial covering the entire add-on listing page where the user can navigate between the screenshots below. Each one has an annotation that can be localized.

- [Screenshot-1](#screenshot-1)
- [Screenshot-2](#screenshot-2)
- [Screenshot-3](#screenshot-3)
- [Screenshot-4](#screenshot-4)
- [Screenshot-5](#screenshot-5)
    
**Detail:** Only annotations can be translated! Images are the same for all languages.    

### Screenshot-1

![](/res/mozilla-addons-store/screenshot-1.png)

`Easily remove annoying ads on the web, including on Facebook and YouTube`

### Screenshot-2

![](/res/mozilla-addons-store/screenshot-2.png)

`Stop websites tracking you and following your online activity`

### Screenshot-3

![](/res/mozilla-addons-store/screenshot-3.png)

`Protects you by blocking viruses and malware hidden in ads`

### Screenshot-4

![](/res/mozilla-addons-store/screenshot-4.png)

`Whitelist websites you want to support`

### Screenshot-5

![](/res/mozilla-addons-store/screenshot-5.png)

`Support websites by allowing Acceptable Ads that aren’t annoying or disruptive`
