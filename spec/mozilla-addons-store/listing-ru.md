# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Этот бесплатный блокировщик рекламы для Firefox блокирует раздражающую рекламу на любых сайтах, включая Facebook и YouTube.`

`Adblock Plus блокирует раздражающую рекламу и поддерживает сайты, по умолчанию разрешая ненавязчивую рекламу (настраивается).`

### Detailed Description

```
<strong>Загрузите бесплатный блокировщик рекламы для Firefox. На сегодняшний день около 500 миллионов загрузок!</strong>

✓ Блокировка назойливой рекламы и всплывающих баннеров
✓ Блокировка рекламных видео на таких сайтах, как YouTube
✓ Ускоренная загрузка интернет-страниц
✓ Снижение риска от "вредоносной" рекламы
✓ Защита конфиденциальности посредством блокировки отслеживания Ваших действий в интернете
✓ Блокировка отслеживания иконок социальных сетей

Дополнительные функции блокировщика рекламы позволяют поддерживать Ваши любимые сайты путем внесения их в список исключений, добавлять или создавать собственные фильтры, а также блокировать отслеживание иконок социальных сетей.

Adblock Plus поддерживает инициативу "Допустимая реклама". Допустимая реклама отображается по умолчанию, что позволяет поддерживать сайты, получающие доход с ненавязчивой рекламы. Эту функцию можно отключить в любое время, если пользователь желает заблокировать всю рекламу. Инициатива дает авторам контента возможность получать монетизацию за свою работу и способствует формированию честной и безопасной среды для пользователей, рекламодателей и авторов. Подробности по ссылке: https://adblockplus.org/acceptable-ads

Загружая и устанавливая данное расширение, вы принимаете наши <a href="https://adblockplus.org/terms">Условия использования</a> и <a href="https://adblockplus.org/privacy">Политику конфиденциальности</a>.
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.    

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`С легкостью блокируйте раздражающую рекламу на сайтах, включая Facebook и YouTube`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Поставьте блок, чтобы сайты не могли отслеживать вас и вашу онлайн активность`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`Защищает вас, блокируя вирусы и вредоносные программы, спрятанные в рекламе`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Добавляйте сайты, которые хотите поддержать, в белый список`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Поддерживайте сайты, разрешая допустимую рекламу, которая не является раздражающей или деструктивной`
