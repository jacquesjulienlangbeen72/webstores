# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)

### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Blokuj irytujące reklamy w witrynach Facebook czy YouTube, korzystając z tej bezpłatnej funkcji blokowania reklam dla Firefox.`

`Adblock Plus blokuje irytujące reklamy i wspiera witryny, domyślnie nie blokując natarczywych reklam (konfigurowalne).`

### Detailed Description

```
<strong>Pobierz bezpłatną wtyczkę Adblocker dla przeglądarki Firefox. Dotychczas pobrano ją już 500 milionów razy!</strong>

✓ Zablokuj irytujące reklamy i wyskakujące okna
✓ Blokuj reklamy wideo w witrynach takich jak YouTube
✓ Przyśpiesz ładowanie stron
✓ Ogranicz ryzyko infekcji złośliwym oprogramowaniem reklamowym
✓ Chroń swoją prywatność, powstrzymując oprogramowanie monitorujące od śledzenia Twojej aktywności online
✓ Blokuj śledzenie ikon mediów społecznościowych

Dodatkowe funkcje blokady reklam umożliwiają łatwe wspieranie ulubionych witryn internetowych poprzez wpisywanie ich na białą listę, dodawanie lub tworzenie własnych filtrów, a także blokowanie śledzenia ikon w mediach społecznościowych.

Adblock Plus wspiera inicjatywę „Akceptowalne reklamy”. Akceptowalne reklamy są wyświetlane domyślnie, co pomaga wspierać witryny internetowe, które opierają się na przychodach z reklam, ale decydują się wyświetlać tylko reklamy nieinwazyjne. Funkcja ta może zostać w każdej chwili wyłączona przez użytkowników, którzy chcą zablokować wszystkie reklamy. Inicjatywa ta pozwala producentom treści na zarabianie na nich, a także pomaga tworzyć środowisko uczciwości i zrównoważonego rozwoju zarówno dla użytkowników, jak i dla reklamodawców czy autorów. Aby dowiedzieć się więcej, odwiedź stronę https://adblockplus.org/acceptable-ads

Pobierając i instalując to rozszerzenie, wyrażasz zgodę na <a href="https://adblockplus.org/terms">Warunki korzystania</a> i <a href="https://adblockplus.org/privacy">Polityka prywatności</a>.
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.     

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`Z łatwością usuwaj irytujące reklamy wyświetlane podczas przeglądania sieci, w tym z Facebooka i YouTube`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Zablokuj witrynom możliwość śledzenia Twojej aktywności online`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`Chroń się! Zablokuj wirusy i złośliwe oprogramowania ukryte w reklamach`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Umieszczaj witryny, które chcesz wspierać na liście dozwolonych`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Wspieraj witryny internetowe, zezwalając na wyświetlanie Akceptowalnych reklam, które nie są irytujące ani uciążliwe`