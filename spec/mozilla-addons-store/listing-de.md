# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Blockiere Werbeanzeigen auf Sites wie Facebook, YouTube und anderen Sites mit diesem kostenlosen Adblocker für Firefox.`

`Adblock Plus blockiert störende Anzeigen und unterstützt Sites, indem unaufdringliche Werbung nicht blockiert wird (einstellbar).`

### Detailed Description

```
<strong>Hole dir den kostenlosen Adblocker für Firefox. Mit bis jetzt fast 500 Millionen Downloads!</strong>

✓ Blocke lästige Werbung und Pop-ups
✓ Blocke Videowerbung auf Websites wie YouTube
✓ Reduziere die Ladezeit von Seiten
✓ Reduziere das Risiko von „Malvertising“ Infektionen
✓ Schütze deine Privatsphäre, indem du Tracker davon abhältst, deine Online-Aktivitäten zu verfolgen
✓ Blocke Tracking in Form von Social-Media-Symbolen

Die zusätzlichen Funktionen des Werbeblockers ermöglichen es dir, deine Lieblingswebsites auf die Whitelist zu setzen, deine eigenen Filter hinzuzufügen oder zu erstellen und das Tracking in Form von Social-Media-Symbolen zu blocken.

Adblock Plus unterstützt die Acceptable Ads Initiative. Acceptable Ads werden standardmäßig angezeigt, um Websites zu unterstützen, die auf Werbeeinnahmen angewiesen sind, und daher nur unauffällige Werbung zeigen. Diese kann von Benutzern, die alle Arten von Werbung blockieren möchten, zu jeder Zeit deaktiviert werden. Die Initiative ermöglicht es den Herstellern von Content, Einnahmen mit ihrer Arbeit zu erzielen, und trägt dazu bei, ein faires und nachhaltiges Umfeld für Nutzer, Werbetreibende und Ersteller zu schaffen. <a href="https://adblockplus.org/acceptable-ads">Mehr dazu lesen</a>

Durch den Download und die Installation dieser Erweiterung stimmst du unseren <a href="https://adblockplus.org/terms">Nutzungsbedingungen</a> und unseren <a href="https://adblockplus.org/privacy">Datenschutzrichtlinien</a> zu.
```

## Screenshots
    
**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.   

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.  

`Entferne einfach störende Werbeanzeigen im Web, auch auf Facebook und YouTube`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Hindere Websites daran, dir nachzuspüren und deinen Online-Aktivitäten zu folgen`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`Schützt dich, indem es Viren und Malware blockiert, die in Werbung versteckt sind`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Setze Websites, die du unterstützen möchtest, auf die Whitelist`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Unterstütze Websites, indem du Acceptable Ads zulässt, die dich nicht ärgern oder stören`
