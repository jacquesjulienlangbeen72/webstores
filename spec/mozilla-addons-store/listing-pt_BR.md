# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Bloqueie anúncios intrusivos em vários sites e videos, usando o bloqueador de anúncios gratuito para Firefox.`

`Adblock Plus bloqueia anúncios irritantes e oferece suporte a sites sem bloquear anúncios não invasivos por padrão (configurável).`

### Detailed Description

```
<strong>Obtenha o Adblocker gratuito para Firefox. Com quase 500 milhões de downloads até hoje!</strong>

✓ Bloqueie anúncios e pop-ups inconvenientes
✓ Bloqueie os anúncios em vídeo apresentados em sites como o YouTube
✓ Acelere o tempo de carregamento de páginas
✓ Reduza o risco de infecções resultantes de "malvertising"
✓ Proteja a sua privacidade impedindo que rastreadores sigam a sua atividade on-line
✓ Bloqueie o rastreamento de ícones de mídia social

Os recursos adicionais do bloqueador de anúncios permitem que você suporte facilmente seus sites favoritos autorizando-os, adicione ou crie seus próprios filtros e bloqueie o rastreamento de ícones de mídia social.

O Adblock Plus suporta a iniciativa de Anúncios Aceitáveis. Os anúncios aceitáveis são exibidos por padrão, o que ajuda a suportar os sites que dependem da receita de publicidade, mas optam por exibir somente anúncios não intrusivos. Essa função pode ser desabilitada a qualquer momento para os usuários que queiram bloquear todos os anúncios. A iniciativa permite que os produtores de conteúdo recebam monetização por seu trabalho e ajuda a criar um ambiente de justiça e sustentabilidade para o usuário, o anunciante como também para o criador. Saiba mais em https://adblockplus.org/acceptable-ads

Ao baixar e instalar esta extensão, você aceita nossos <a href="https://adblockplus.org/terms">Termos de Uso</a> e nossa <a href="https://adblockplus.org/privacy">Política de Privacidade</a>.
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.    

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`Remova anúncios irritantes da Web facilmente, incluindo no Facebook e no YouTube`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Não deixe que sites rastreiem você e sigam sua atividade on-line`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`Protege você, bloqueando vírus e malwares que se escondem em anúncios`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Sites da lista branca que você deseja apoiar`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Dê seu suporte a sites permitindo anúncios aceitáveis, que não são irritantes ou inconvenientes`