# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)

### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`このFirefox用の無料広告ブロッカーで、FacebookやYouTubeなどのサイトやその他のウェブサイトの迷惑な広告をブロックしましょう。`

`Adblock Plusはすべての迷惑な広告をブロックし、控えめな広告をデフォルトで許可することでウェブサイトをサポートします（設定可能）。`

### Detailed Description

```
<strong>Firefox用Adblockerを無料で取得してください。これまでのダウンロード回数は約5億回！</strong>

✓ 迷惑な広告やポップアップをブロック
✓ YouTubeなどのサイトにおける動画広告をブロック
✓ ページの読み込み時間を短縮
✓ 「マルバタイジング （悪意のある広告）」による感染リスクを軽減
✓ あなたのオンラインアクティビティをトラッカーがフォローすることを禁じることでプライバシーを保護
✓ ソーシャルメディアボタンによるユーザー追跡をブロック

このAdblockerの追加機能により、お気に入りのウェブサイトをホワイトリストに入れたり、自分自身のフィルタに追加/作成したり、ソーシャルメディアのアイコンによるトラッキングをブロックできます。これによってお気に入りのウェブサイトを簡単にサポートできます。

Adblock Plusは「控えめな広告」イニシアチブを推進しています。「控えめな広告」はデフォルトで表示され、広告収入を当てにしているけれども、おしつけがましくない広告のみを表示するウェブサイトをサポートします。すべての広告をブロックしたいユーザーの場合、いつでもこれは無効化できます。このイニシアチブはコンテンツ制作者が自分の作品でマネタイゼーション (金銭化) を実現することを可能にするもので、ユーザー、広告主、そしてクリエーターに対して一様に公正さと持続可能性のある環境を作り出す上で役立ちます。詳細は<a href="https://adblockplus.org/acceptable-ads">https://adblockplus.org/acceptable-ads</a>を参照

あなたは、この拡張機能をダウンロードしてインストールすることで当社の <a href="https://adblockplus.org/terms">利用規約</a> および当社の <a href="https://adblockplus.org/privacy">プライバシーポリシー</a> に同意したことになります。
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.    

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`FacebookやYouTubeを含むウェブ上の迷惑な広告を簡単削除`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`ウェブサイトのトラッキングとオンライン活動の追跡を停止`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`広告に隠されたウイルスやマルウェアをブロックして保護`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`サポートするウェブサイトをホワイトリストに登録`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`迷惑または妨害的ではない、控えめな広告を許可して、ウェブサイトをサポート`