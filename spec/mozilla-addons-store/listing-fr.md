# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)


### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Bloque les publicités gênantes sur Facebook, YouTube et d'autres sites grâce à ce bloqueur de publicité pour Firefox.`

`Adblock Plus bloque les publicités gênantes et aide les sites en ne bloquant pas les publicités non intrusives (configurable).`

### Detailed Description

```
<strong>En téléchargeant le logiciel Adblock Plus vous acceptez que si vous utilisez Adblock Plus pour visiter un site Internet en violation de toute obligation ou droit de quelque nature que ce soit en relation avec ce site Internet, en aucun cas EYEO ne pourra être tenu pour responsable envers vous ou tout autre tiers pour toute perte ou dommage (y compris, sans y être limité, les dommages pour perte de chances et perte de bénéfices) découlant directement ou indirectement de votre utilisation de ce logiciel.</strong>

<strong>Obtenez gratuitement Adblock pour Firefox. Près de 500 millions de téléchargements effectués à ce jour !</strong>

✓ Bloquez les publicités et fenêtres pop-up gênantes
✓ Bloquez les vidéos publicitaires sur les sites tels que YouTube
✓ Accélérez le temps de chargement des pages Web
✓ Réduisez les risques de virus par des publicités malveillantes
✓ Préservez votre vie privée en empêchant les traqueurs de pister vos activités en ligne
✓ Bloquez le pistage par les icônes des réseaux sociaux

Les fonctionnalités supplémentaires du bloqueur de publicités vous permettent de soutenir aisément vos sites Web préférés en les ajoutant à la liste blanche, d'ajouter ou de créer vos propres filtres, et de bloquer les icônes de pistage des réseaux sociaux.

Adblock Plus soutient l'initiative Publicité acceptable (Acceptable Ads - AA) Les Publicités acceptables sont affichées par défaut afin de soutenir les sites Web dont le financement dépend des recettes publicitaires, mais en faisant le choix d'afficher uniquement les publicités non intrusives. Cette fonctionnalité peut être désactivée à tout moment pour les utilisateurs qui souhaitent bloquer toutes les publicités. Cette initiative permet aux éditeurs de contenus de  monétiser leur travail et contribue à créer un environnement d'équité et de durabilité à la fois pour les utilisateurs, les annonceurs et les créateurs. <a href="https://adblockplus.org/acceptable-ads">En savoir plus</a>

En téléchargeant et en installant cette extension, vous acceptez nos <a href="https://adblockplus.org/terms">Conditions d'utilisation</a> et notre <a href="https://adblockplus.org/privacy">Politique de confidentialité</a>.
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.      

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`Supprimez facilement les publicités gênantes sur Internet y compris sur Facebook et sur YouTube`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Désactivez le suivi des sites web et de votre activité en ligne`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`Vous protège en bloquant les virus et les programmes malveillants cachés dans les publicités`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Mettez sur liste blanche les sites web que vous voulez soutenir`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Soutenez des sites en autorisant des Publicités Acceptables qui ne sont ni gênantes ni intrusives`
