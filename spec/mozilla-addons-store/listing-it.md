# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)

### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Blocca gli annunci fastidiosi su Facebook, YouTube e tanti altri, usando questo ad blocker gratuito per Firefox.`

`Adblock Plus blocca tutti gli annunci fastidiosi e supporta i siti Web permettendo solo annunci non invasivi (configurabile).`

### Detailed Description

```
<strong>Ottieni un Adblocker gratuito per Firefox, che ad oggi ha quasi 500 milioni di download!</strong>

✓ Blocca annunci fastidiosi e pop-up
✓ Blocca gli annunci video su siti come YouTube
✓ Accelera i tempi di caricamento delle pagine
✓ Riduci il rischio di "malware" da annunci dannosi
✓ Proteggi la tua privacy e impedisci ai tracker di monitorare la tua attività online
✓ Blocca il tracking delle icone dei social media

L'adblocker ti consente inoltre di supportare i tuoi siti web preferiti con un semplice meccanismo di whitelisting, di aggiungere o creare i tuoi filtri personali e di bloccare il tracking delle icone dei social media.

Adblock Plus sostiene l'iniziativa Annunci Accettabili. Gli Annunci Accettabili sono mostrati per impostazione predefinita, in modo da supportare i siti web che ottengono ricavi dalla pubblicità, ma che scelgono di mostrare solo gli annunci non invadenti. Se desideri bloccare tutti gli annunci, puoi disabilitare questa funzionalità in qualsiasi momento. L'iniziativa aiuta i produttori di contenuti a monetizzare il proprio lavoro e contribuisce a creare un ambiente di equità e sostenibilità per gli utenti, gli inserzionisti ed i creatori. Per maggiori informazioni, visita https://adblockplus.org/acceptable-ads

Scaricando e installando questa estensione, accetti i nostri <a href="https://adblockplus.org/terms">Termini di Utilizzo</a> e la nostra <a href="https://adblockplus.org/privacy">Privacy Policy</a>.
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.    

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`Rimuovi facilmente le fastidiose pubblicità Web, anche su Facebook e YouTube`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Blocca il tracciamento dei siti Web che controllano la tua attività online`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`Offre protezione bloccando i virus e malware nascosti negli annunci`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Inserisci nella lista i siti Web abilitati che desideri supportare`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Supporta i siti Web consentendo solo annunci accettabili, non fastidiosi e non invasivi`


