# Mozilla Add-on Store Listing Adblock Plus

1. [Listing Search Result](#listing-search-result)
2. [Listing Overview](#listing-overview)

## Listing Search Result

Preview (English):

![](/res/mozilla-addons-store/search-result.png)

1. [Title](#title)
1. [Summary](#summary)

## Listing Overview

Preview (English):

![](/res/mozilla-addons-store/listing.png)

1. [Title](#title)
1. [Summary](#summary)
1. [Detailed Description](#detailed-description)
1. [Screenshots](#screenshots)

### Title

`Adblock Plus`

### Summary

**Detail:** The translated text differs from the English version. Due to character limit, we had to translate a shorter sentence for the first string. [See details >](../mozilla-addons-store/listing-en_US.md#summary)

`Blokkolja a zavaró hirdetéseket az ingyenes Firefox hirdetésblokkolóval pl. Facebookon, YouTube-on stb.`

`Az Adblock Plus blokkolja a zavaró hirdetéseket, támogatja webhelyeket: alapértelmezésben nem blokkolja az diszkrét hirdetéseket (beállítható).`

### Detailed Description

```
<strong>Szerezd be az ingyenes Adblockert a Firefox böngészőhöz. Eddig közel 500 millió letöltés!<strong>

✓ Blokkolja a zavaró hirdetéseket és a felugró ablakokat
✓ Blokkolja a videóhirdetéseket az olyan oldalakon, mint a YouTube
✓ Gyorsítja a weboldalak betöltési sebességét
✓ Csökkenti a „rosszindulatú hirdetések” okozta fertőzések kockázatát
✓ Védi a személyes adatokat: megakadályozza az online tevékenységek követését
✓ Blokkolja a közösségimédia-ikonok nyomon követését

Az Adblocker további funkciói lehetővé teszik, hogy engedélyezd kedvenc webhelyeidet, saját szűrőket hozz létre vagy állíts be és blokkold a közösségimédia-ikonok nyomon követését.

Az Adblock Plus támogatja az elfogadható hirdetések (Acceptable Ads) kezdeményezést. Az elfogadható hirdetések alapértelmezés szerint láthatók, ami segíti a hirdetési bevételekkel működő weboldalak támogatását. A tolakodó hirdetések azonban nem jelennek meg. A felhasználók bármikor letilthatják ezt a beállítást, ha az összes hirdetést blokkolni szeretnék. A kezdeményezés bevételi forrást jelent a tartalomszolgáltatóknak, és tisztességes és fenntartható környezetet teremt a felhasználók, a hirdetők és a szolgáltatók számára. A https://adblockplus.org/acceptable-ads oldalon többet megtudhatsz a kezdeményezésről.

A bővítmény letöltésével és telepítésével elfogadod a <a href="https://adblockplus.org/terms">Használati feltételeinket</a> és az <a href="https://adblockplus.org/privacy">Adatvédelmi szabályzatunkat</a>.
```

## Screenshots

**Detail:** Only annotations can be translated! Images are the same for all languages. 

See [en_US](../mozilla-addons-store/listing-en_US.md#screenshots) instead.   

### Screenshot-1

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-1) instead.

`Távolítsd el könnyedén a bosszantó hirdetéseket az interneten, a Facebookot és YouTube-ot is beleértve`

### Screenshot-2

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-2) instead.

`Akadályozd meg, hogy a weboldalak nyomon kövessenek és kövessék az internetes tevékenységeidet`

### Screenshot-3

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-3) instead.

`A rejtett hirdetésekben lévő vírusok és rosszindulatú szoftverek blokkolása által nyújt védelmet`

### Screenshot-4

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-4) instead.

`Engedélyezett webhelyek, amiket támogatni szeretnél`

### Screenshot-5

For the image, see [en_US](../mozilla-addons-store/listing-en_US.md#screenshot-5) instead.

`Támogasd a weboldalakat a nem zavaró Elfogadható Hirdetések engedélyezésével`
