# README

Functional specification for the ABP Chrome Web Store listing.

## License

Copyright (C)  2017  eyeo GmbH.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".

[GNU Free Documentation License](/COPYING)

## File structure

- `/spec/` markdown source files go here
- `/res/` images and other resources go here

## Local development preview

You can preview your local changes using [grip](https://github.com/joeyespo/grip)

```bash
$ cd specrepo
$ grip
 * Running on http://localhost:6419/
```

Then point your browser to the file you want to preview <http://localhost:6419/spec/PRODUCT/filename.md>

## Resources

- [How to spec](/eyeo/specs/spec/blob/master/doc/how-to-spec.md) Guide on how to write functional specifications
- [How to setup grip and bitbucket on osx](/eyeo/specs/spec/blob/master/doc/grip-bitbucket-osx.md) Guide on how to setup [grip](https://github.com/joeyespo/grip) and [github](https://github.com) on osx
- [How to edit the spec using git](/eyeo/specs/spec/blob/master/doc/git-basics.md) Guide on how to edit the specification using git
- [How to link to a specific version](/eyeo/specs/spec/blob/master/doc/git-basics.md#how-to-link-to-a-specific-version) How to get a link to a specific version (for use in tickets)
- [Working with WIP commits](/eyeo/specs/spec/blob/master/doc/git-basics.md#working-with-wip-commits) How to temporarily commit you changes to work on another branch
- [Markdown style guide](http://www.cirosantilli.com/markdown-style-guide/) Markdown styleguide to help you produce better markdown files
